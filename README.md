# Netsuite with usability fixes (UserStyle)

## Warning!

These fixes will break when Netsuite code is updated. The styles have been put together with care as not to break anything, no guarantees are given. These are tested with very limited user rights to serve the most Siilis, so if you have more rights in Netsuite, things may look weird.

Remember, you can always disable these styles to go back into the regular Netsuite experience.

If you notice any problems, please let us know in the issues here or by joining #netsuite_userstyle slack channel.

## Installation

1. Check that you have the [Stylus](https://add0n.com/stylus.html) extension installed in your Chrome browser (Might work in Firefox too, but this project does not yet support Firefox)
2. Restart your browser after installing Stylus.
3. Open [netsuite.user.styl](https://gitlab.siilicloud.com/mikko.tapionlinna/netsuite-userstyle/raw/master/netsuite.user.styl) with the Stylus extension enabled.
4. Install style when Stylus detects the netsuite.user.styl as a proper userstyle.
5. Enjoy slightly better Netsuite

## Features

The main idea of this project is to improve the user experience of the Netsuite tool at Siili.

Improvements include:

* Cleaner theme with real Siili brand colors.
* Dark mode for those late nights and early mornings (contributor: Yves)
* Minor usability fixes, like underlining certain links
* Option to clarify interface with helpful texts.
    * Weekly hour sums explained
    * Button label for flex time calculation
    * Note about Netsuite team fixing the Flex time calculator within the widget
    * Home button label to go with the icon
    * Better relogin help text
    * Warning for users trying to login with a password instead of SSO
    * Clarify next and prev week button labels when not in edit mode as it works differently
* Reformat Flex Time sum to look prettier
* Hide piechart tools when not in use
* Allow using three column view with a narrower screen size
* Convert ALL CAPS to a more sane capitalisation
* Line up week view summed hours better with the columns they are input in.
* Hilight submit button and clarify its label
* Option to use emojis in the interface for smaller footprint if your OS supports it.
* Allow report dropdowns to be narrow when used in dashboard for more sane layout
* Color relogin button as disabled to avoid it being used for relogin
* Add FAQ section to the hour input page to handle the most common issues seen on #netsuitesupport slack channel
* Make the Siili logo work as a link back to home page as it should
* Warn the user when their data has not yet been saved on clicking save
* Warn the user when their data has not yet been submitted on clicking submit
* Add a small footer notification about this style being used.
* Option to make things that are hidden behind mouse hover visible or hinted:
    * Show `Time tracking > Track time > List`-type links always instead of on hover.
    * Show calendar popup button in Time Tracking view always
    * Some labels have "help popup", underline them to hint about it.
* Option to show month in time tracking table days
* Try to force submit and save buttons to keep order

## Support

Please use the [issues](https://gitlab.siilicloud.com/mikko.tapionlinna/netsuite-userstyle/issues) here to report any problems or ideas you have for this style.

You can also join Siili slack channel #netsuite_userstyle to discuss.

## Licence

MIT